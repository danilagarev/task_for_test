from django.urls import path
import infoapp.views as infoapp

urlpatterns = [
    path('load_average/', infoapp.load_average, name="load"),
    path('ram/', infoapp.ram, name="ram"),
    path('auth/', infoapp.auth, name="auth"),
]

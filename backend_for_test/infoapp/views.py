from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
import psutil
from psutil._common import bytes2human

# @login_required(login_url="auth")
def load_average(request):
    load = round(psutil.getloadavg()[0] / psutil.cpu_count(logical=False) * 100, 2)
    content = {
        'load': load,
    }
    return render(request, 'infoapp/load_average.html', context=content)

# @login_required(login_url="auth")
def ram(request):
    total_ram = bytes2human(psutil.virtual_memory()[0])
    available_ram = bytes2human(psutil.virtual_memory()[1])
    used_ram = bytes2human(psutil.virtual_memory()[3])
    free_ram = bytes2human(psutil.virtual_memory()[4])
    content = {
        "total_ram": total_ram,
        "available_ram": available_ram,
        "used_ram": used_ram,
        "free_ram": free_ram,
    }
    return render(request, "infoapp/ram.html", context=content)


def auth(request):
    return render(request, "infoapp/auth.html")